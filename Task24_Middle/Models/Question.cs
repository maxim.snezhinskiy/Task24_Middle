﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Question: BaseEntity
    {
        public string QuestionTitle { get; set; }
        public List<string> Text { get; set; }
    }
}