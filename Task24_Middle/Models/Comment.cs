﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Comment : BaseEntity
    {
        public string Name { get; set; }
        public DateTime DatePosted { get; set; }
        public string CommentText { get; set; }
    }
}