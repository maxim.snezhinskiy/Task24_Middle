﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Task24_Middle.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString CreateListRadio(this HtmlHelper html, List<string> data, string name)
        {
            var ul = new TagBuilder("ul");
            StringBuilder result = new StringBuilder();
            foreach (var item in data)
            {
                var li = new TagBuilder("li");
                var input = new TagBuilder("input");
                input.MergeAttribute("type", "radio");
                input.MergeAttribute("name", name);
                input.MergeAttribute("value", item);
                li.InnerHtml = input.ToString();
                li.InnerHtml += item;
                result.Append(li);
            }
            ul.InnerHtml = result.ToString();
            result.Clear();
            result.Append(ul.ToString());
            return new MvcHtmlString(result.ToString());
        }
    }
}