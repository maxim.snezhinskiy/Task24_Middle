﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class HomeController : Controller
    {


        static List<Article> articles = new List<Article>() {
            new Article{Id = 1, Title = ".NET", DatePosted = DateTime.Now, Content =$".NET is a free, cross-platform, open source developer platform for building many different types of applications." +
                $"With .NET, you can use multiple languages, editors, and libraries to build for web, mobile, desktop, games, and IoT."},
            new Article{Id = 2, Title = "Frontend", DatePosted =  new DateTime(2015, 5, 25), Content =$"Front-end web development is the practice of converting data to a graphical interface, " +
                $"through the use of HTML, CSS, and JavaScript, so that users can view and interact with that data. " },
            new Article{Id = 3, Title = "Machine Learning", DatePosted =  new DateTime(2004, 3, 2), Content =$"Machine learning -Machine learning and data mining. v. t. e. Machine learning (ML)" +
                $" is the study of computer algorithms that improve automatically through experience and by the use of data. It is seen as a part of artificial intelligence." +
                $" Machine learning algorithms build a model based on sample data, known as \"training data\". "}
        };

        public ActionResult Index()
        {

            ViewBag.Articles = articles;
            return View();
        }

    }
}