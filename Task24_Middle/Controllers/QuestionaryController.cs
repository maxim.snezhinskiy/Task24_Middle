﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class QuestionaryController : Controller
    {
        static Question mainLang = new Question()
        {
            Id = 1,
            QuestionTitle= "What is Your main programming language?",
            Text = new List<string>()
            {
                "C#",
                "Java",
                "Javascript"
            }
        };

        static Question lang = new Question()
        {
            Id=2,
            QuestionTitle = "What is Your native language?",
            Text = new List<string>()
            {
                "English",
                "Ukrainian",
                "Other"
            }
        };

        static List<QuestionaryResult> qResutl = new List<QuestionaryResult>();

        [HttpGet]
        public ActionResult Questionary()
        {
            ViewBag.LangQuiz = lang;
            ViewBag.MainLang = mainLang;
            return View();
        }

        [HttpPost]
        public ActionResult Questionary(string LangQuiz, bool likeProgramming, string MainLang, string EmailInput)
        {
            if (String.IsNullOrEmpty(EmailInput))
            {
                ModelState.AddModelError("Email", "Input email");
            }

            if (ModelState.IsValid)
            {
                qResutl.Add(new QuestionaryResult { Id = 1, Answer = LangQuiz, QuestionId = 1 });
                qResutl.Add(new QuestionaryResult { Id = 2, Answer = MainLang, QuestionId = 2 });

                ViewBag.Lang = LangQuiz;
                ViewBag.LikeProgramming = likeProgramming ? "Yes" : "No";
                ViewBag.MainLang = MainLang;
                ViewBag.Email = EmailInput;
            }
            return View("QuestionaryResult");
        }
    }
}