﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class GuestController : Controller
    {
        //static List<(string name, DateTime date, string comment)> comments = new List<(string name, DateTime date, string comment)>
        //    {
        //        ("Mike Brown", new DateTime(2005, 5, 8) , "Not bad"),
        //        ("John Williams", new DateTime(2020, 4, 17), ".NET is the best platform"),
        //        ("Jack Daniels", new DateTime(2021, 7, 3) , "I hate JS"),
        //    };


        static List<Comment> comments = new List<Comment>()
        {
            new Comment{Id=1, Name = "Mike Brown", DatePosted = new DateTime(2005, 5, 8), CommentText = "Not bad"},
            new Comment{Id = 2, Name = "John Williams", DatePosted =new DateTime(2020, 4, 17), CommentText =  ".NET is the best platform"},
            new Comment{Id = 3, Name ="Jack Daniels", DatePosted= new DateTime(2021, 7, 3), CommentText= "I hate JS" }
        };

        [HttpGet]
        public ActionResult FeedBack()
        {

            ViewBag.FeedBack = comments;
            return View();
        }

        [HttpPost]
        public ActionResult FeedBack(FormCollection form)
        {
            if (form["Name"] == String.Empty || form["Name"].Contains(" "))
            {
                ModelState.AddModelError("Name", "Please enter your name");
            }
            if (form.Get("CommetText") == String.Empty || form.Get("CommentText").StartsWith(" "))
            {
                ModelState.AddModelError("Comment", "Comment can't be empty");
            }

            if (ModelState.IsValid)
            {
                comments?.Add(new Comment { Id = comments.Max(f => f.Id) + 1, Name = form["Name"], CommentText = form["CommentText"], DatePosted = DateTime.Now });
            }
            ViewBag.FeedBack = comments;
            return View();
        }


    }
}